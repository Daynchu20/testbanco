package dto;

import java.util.List;

public class ItemResult {
    String id;
    Double price;
    String title;
    List<ItemAttributes> attributes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ItemAttributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<ItemAttributes> attributes) {
        this.attributes = attributes;
    }

    public String getType(){
        return attributes.stream()
                .filter(s -> "BRAND".equals(s.getId()))
                .map(attributes ->
                        attributes.getValues().stream()
                                .map(itemValues -> itemValues.getName())
                                .findFirst().orElse("-"))
                .findFirst().orElse("-");
    }
}
