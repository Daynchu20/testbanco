package dto;

import java.util.List;

public class MeliResponse {

    List<ItemResult> results;

    public List<ItemResult> getResults() {
        return results;
    }

    public void setResults(List<ItemResult> results) {
        this.results = results;
    }
}
