package dto;

import java.util.List;

public class ItemAttributes {
    String id;
    String name;
    List<ItemValues> values;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ItemValues> getValues() {
        return values;
    }

    public void setValues(List<ItemValues> values) {
        this.values = values;
    }
}
