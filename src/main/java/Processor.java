import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import dto.ItemResult;
import dto.MeliResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Processor {
    public static ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static void executeQuery() throws IOException {
        String url = "https://api.mercadolibre.com/sites/MLA/search?category=MLA1763&offset=%d&ITEM_CONDITION=2230284";
        List<ItemResult> contentList = new ArrayList<>();

        int pagesize = 50;
        int k = 900 / pagesize;
        OkHttpClient client = new OkHttpClient();


        //Get and transform all the data from meli
        for (int i = 0; i < k; i++) {
            Request request = new Request.Builder()
                    .url(String.format(url, i * pagesize))
                    .method("GET", null)
                    .build();
            Response response = client.newCall(request).execute();
            String MyResult = response.body().string();
            MeliResponse meliResponse = objectMapper.readValue(MyResult, MeliResponse.class);

            contentList.addAll(meliResponse.getResults());
        }

        //Calculate the average and print it
        Map<String, Double> averagePrices = contentList.stream()
                .collect(Collectors.groupingBy(ItemResult::getType,
                        Collectors.averagingDouble(ItemResult::getPrice)));

        averagePrices.forEach((brand, average) -> System.out.println("Marca: " + brand +" - Promedio: " + average));
    }

}
